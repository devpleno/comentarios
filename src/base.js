import firebase from 'firebase'
import Rebase from 're-base'

const config = {
    apiKey: "AIzaSyAUKx8KAJRZ-HScJRYExcJnL1KcdaIITto",
    authDomain: "reactjs-3a44a.firebaseapp.com",
    databaseURL: "https://reactjs-3a44a.firebaseio.com",
    storageBucket: "reactjs-3a44a.appspot.com",
    messagingSenderId: "334092784672"
}

const app = firebase.initializeApp(config)
const db = firebase.database(app)
const base = Rebase.createClass(db)

export const providers = {
   'facebook': new firebase.auth.FacebookAuthProvider()
}

export const auth = app.auth()
export default base
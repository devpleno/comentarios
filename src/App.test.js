import React from 'react';
import App from './App';

import { shallow, mount, render } from 'enzyme'

describe('<App />', () => {

  const base = {
    syncState: jest.fn()
  }

  const auth = {
    syncState: jest.fn(),
    onAuthStateChanged: {
      syncState: jest.fn()
    }
  }

  const providers = {
    syncState: jest.fn()
  }

  it('renders without crashing', () => {
    const wrapper = shallow(<App base={base} auth={auth} providers={providers} />)
    expect(wrapper.length).toBe(1)
  });

  it('should have .container class', () => {
    const wrapper = shallow(<App base={base} auth={auth} providers={providers} />)
    expect(wrapper.is('.container')).toBe(true)
  });

  // it('outputs the App', () => {
  //   const wrapperShallow = shallow(<App base={base} />)
  //   const wrapperMount = mount(<App base={base} />)
  //   const wrapperRender = render(<App base={base} />)

  //   console.log(wrapperShallow.debug())
  //   console.log(wrapperMount.debug())
  //   console.log(wrapperRender.html())
  // });

  it('show comment', () => {
    const wrapper = shallow(<App base={base} auth={auth} providers={providers} />)
    expect(wrapper.find('Comments').length).toBe(1)
  });

  it('new comment', () => {
    const wrapper = shallow(<App base={base} auth={auth} providers={providers} />)
    expect(wrapper.find('NewComment').length).not.toBe(1)
  });

  it('add comment', () => {
    const wrapper = mount(<App base={base} auth={auth} providers={providers} />)

    wrapper.instance().postNewComment({ comment: 'test 1'})
    wrapper.instance().postNewComment({ comment: 'test 2'})
    wrapper.instance().postNewComment({ comment: 'test 3'})

    const comments = Object.keys(wrapper.instance().state.comments)

    expect(comments.length).toBe(3)
  });
})
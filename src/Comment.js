import React from 'react'

const Comment = props =>
    <p className="alert alert-dark">
        <b>{props.comment.user.name} disse:</b> {props.comment.comment}        
    </p>

export default Comment;